---
hide:
  - navigation
  - toc
---

# Blender Developer Documentation

!!! warning "Work in Progress!"

    This is a new (experimental) repository for Markdown based developer
    documentation, using
    [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

    __The official platform for developer documentation is still
    [wiki.blender.org](https://wiki.blender.org/).__

## Building this Documentation Offline

=== "Windows"
    **Install required software and set up Git LFS:**

    1. Download the [Python installation package](https://www.python.org/downloads/) for Windows.
    2. Install Python with the installation wizard. **Make sure to enable the "Add Python to PATH" option**.
    3. Download and install [Git for Windows](https://git-scm.com/download/win).

    Open a command line window and run the following command:
    ```shell
    git lfs install
    ```

=== "macOS"
    Install required software:  [PIP](https://pip.pypa.io/en/latest/installation/), [Git](https://git-scm.com/download/mac) and [Git LFS](https://git-lfs.com/).
    When using [Homebrew](https://brew.sh/), run the following commands in the terminal:
    ```sh
    python3 -m ensurepip
    brew install git git-lfs
    git lfs install
    ```

=== "Linux"
    Install Python, PIP, Git and Git LFS using your package manager:

    === "Debian"
        ```sh
        sudo apt install python3 python3-pip git git-lfs
        git lfs install --skip-repo
        ```
    === "Redhat/Fedora"
        ```sh
        sudo yum install python python-pip git git-lfs
        git lfs install --skip-repo
        ```
    === "Arch Linux"
        ```sh
        sudo pacman -S python python-pip git git-lfs
        git lfs install --skip-repo
        ```


**Clone the documentation sources:**
```sh
git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
```
This will clone the sources into a `developer-docs` directory inside the current
one.

**Install all dependencies, such as
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/):**
```sh
python -m pip install -r requirements.txt
```

**Build this documentation with live reloading:**
```sh
mkdocs serve
```

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.
